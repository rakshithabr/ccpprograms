#include<stdio.h>
void swap(int *x,int *y )
{
  int temp;
  temp=*x;
  *x=*y;
  *y=temp;
}
int main()
{
  int a,b,*x,*y;
  x=&a;
  y=&b;
  printf("Enter two numbers\n");
  scanf("%d%d",x,y);
  printf("Before swapping a=%d and b=%d\n",a,b);
  swap(&a,&b);
  printf("After swapping a=%d and b=%d\n",a,b);
  return 0;
}
