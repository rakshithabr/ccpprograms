#include<stdio.h>
int main()
{
    int a[4][3];
    int tsales, psales;
    int i, j;
    printf("Enter the array elememts\n");
    for(i=0; i<4;i++)
    {
        for(j=0; j<3; j++)
        {
            scanf("%d", &a[i][j]);
        }
    }
    
    printf("Total sales of each salesman:\n");
    for(i=0; i<4;i++)
    {
        tsales=0;
        for(j=0; j<3;j++)
        {
            tsales+=a[i][j];
        }
        printf("total sales of salesman %d =%d\n",(i+1),tsales);
    }
    
    printf("Total sales of each product:\n");
    for(j=0; j<3; j++)
    {
        psales=0;
        for(i=0; i<4; i++)
        {
           psales+=a[i][j]; 
        }
        printf("total sales of product %d =%d\n",(j+1),psales);
    }
    
    return 0;
}