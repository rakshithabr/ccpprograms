#include<stdio.h>
int main()
{
	int *x,*y;
	int a,b,add,diff,product,rem;
	float div;
	x=&a;
	y=&b;
	printf("Enter any two integers\n");
	scanf("%d%d",x,y);

	add=*x+*y;
	printf("Sum=%d\n",add);

	diff=*x-*y;
	printf("Difference=%d\n",diff);

	product=(*x)*(*y);
	printf("Product=%d\n",product);

	div=(float)(*x)/(*y);
	printf("%d divided by %d gives %f\n",*x,*y,div);

	rem=(*x)%(*y);
	printf("Remainder=%d\n",rem);
	return 0;
}