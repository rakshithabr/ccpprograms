#include<stdio.h>
#include<math.h>
int main()
{
    float a,b,c,r1,r2;
    printf("Enter the coefficients of the quadratic equation\n");
    scanf("%f%f%f",&a,&b,&c);
    if(a!=0)
    {
        float dis;
        dis=(b*b)-(4*a*c);
        if(dis==0)
        {
            printf("The roots are real and equal\n");
            r1=-b/(2*a);
            r2=r1;
            printf("The roots of the equation are %f and %f\n",r1,r2);
        }
        else if(dis>0)
        {
            printf("The roots are real and distinct\n");
            r1=(-b+sqrt(dis))/(2*a);
            r2=(-b-sqrt(dis))/(2*a);
            printf("The roots are %f and %f\n",r1,r2);
        }
        else 
        {
            printf("The roots are imaginary and conjugate\n");
            printf("The roots are %f+i(%f) and %f-i(%f)\n",-b/(2*a),sqrt(fabs(dis))/(2*a),-b/(2*a),sqrt(fabs(dis))/(2*a));
        }
    }
    else
    {
        printf("The given equation is not a quadratic equation\n");
    }
    return 0;
}



